import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { AdService } from '../ad.service';

@Component({
  selector: 'app-singlead',
  templateUrl: './singlead.component.html',
  styleUrls: ['./singlead.component.scss']
})
export class SingleadComponent implements OnInit {

  ad;
  constructor(private adService: AdService, private route: ActivatedRoute) { }

  ngOnInit() {
    const id = this.route.snapshot.params['id'];
    this.ad = this.adService.getAdById(id);
  }

}
