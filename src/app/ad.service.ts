import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';


@Injectable()
export class AdService {

  constructor(private http:HttpClient) {
    this.getAds();
  }

  annonces: any[] = [];

  getAds() {
    let observable = this.http.get<any>('https://lecoinbon-6ae0b.firebaseio.com/ads.json');
    let observer = {
      next: (data)=> {
        this.annonces = Object.keys(data).map(function(key) {
          return data[key];
        });
        console.log(this.annonces)
      },
      error: (error)=> console.log(error),
      complete: ()=> console.log('oui')
    }
    observable.subscribe(observer);
  }


  publier(){
    for(let annonce of this.annonces){
      annonce.status = "Publiée";
    }
  }

  getAdById(id: number){
    for(let annonce of this.annonces){
      if(annonce.id == id ){
        return annonce;
      }
    }
  }

  publierUn(index: number){
    this.annonces[index].status = 'Publiée'
  }

  brouillonUn(index: number){
    this.annonces[index].status = 'Brouillon'
  }

  desactiverUn(index: number){
    this.annonces[index].status = 'Désactivée'
  }

  countPublish(){
    var count = null;
    for(let annonce of this.annonces){
      if(annonce.status == "Publiée"){
        count++;
      }
    }
    return count
  }

  getHighestId(){
    var id_array: number[] = [];
    for(let annonce of this.annonces){
      console.log(annonce.id);
      id_array.push(annonce.id);
    }
    return Math.max(...id_array);
  }

  addAd(ad){
    let observable = this.http.post<any>('https://lecoinbon-6ae0b.firebaseio.com/ads.json',ad);
    let observer = {
      next: (data)=> console.log(data),
      error: (error)=> console.log(error),
      complete: ()=> this.getAds()
    }
    observable.subscribe(observer);

  }
}
