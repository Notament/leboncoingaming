import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';


@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  constructor(private authService: AuthService) { }

  isAuth: boolean;

  ngOnInit() {
    this.isAuth = this.authService.isAuth;
  }

  login(){
    this.authService.Login().then(
      () => {
        alert('Connected');
        this.isAuth = this.authService.isAuth;
      }
    )
  }

  logout(){
    this.authService.Logout();
    this.isAuth = this.authService.isAuth;
    alert('Deconnected');
  }

}
