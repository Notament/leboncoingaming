import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AdService } from '../ad.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-ad',
  templateUrl: './add-ad.component.html',
  styleUrls: ['./add-ad.component.scss']
})
export class AddAdComponent implements OnInit {

  constructor(private adService: AdService, private router:Router) { }

  ngOnInit() {
  }

  onSubmit(form: NgForm){
    let ad = form.value
    ad.date = new Date;
    ad.id = this.adService.getHighestId() + 1;
    this.adService.addAd(ad);

    this.router.navigate(['/ads']);

  }

}
