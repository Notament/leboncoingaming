import { Component, OnInit,Input } from '@angular/core';
import { AdService } from '../ad.service'


@Component({
  selector: 'app-ad',
  templateUrl: './ad.component.html',
  styleUrls: ['./ad.component.scss']
})
export class AdComponent implements OnInit {
  @Input() annonce: string;
  @Input() status: string;
  @Input() date: Date;
  @Input() price: number;
  @Input() index: number;
  @Input() id: number;
  constructor(private adService: AdService) { }

  getStatusColor(){
    switch (this.status){
      case 'Brouillon':
        return 'gray';
      case 'Publiée':
        return 'green';
      case 'Désactivée':
        return 'red';
    }
  }

  PublierUn(){
    this.adService.publierUn(this.index);
  }

  BrouillonUn(){
    this.adService.brouillonUn(this.index);
  }

  DesactiverUn(){
    this.adService.desactiverUn(this.index);
  }

  ngOnInit(){
  }

}
