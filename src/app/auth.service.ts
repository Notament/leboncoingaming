import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {

  constructor() { }

  isAuth = false;

  Login(){
    return new Promise((resolve, reject)=>{
      setTimeout(()=>{
          this.isAuth = true;
          resolve(true);
        },2000)
      }
    )
  }

  Logout(){
    this.isAuth = false;
  }

}
