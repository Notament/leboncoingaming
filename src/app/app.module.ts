import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AdComponent } from './ad/ad.component';

import { AdService } from './ad.service';
import { AuthService } from './auth.service';
import { AuthGuard} from './auth-guard.service'
import { CounterComponent } from './counter/counter.component';
import { AuthComponent } from './auth/auth.component';
import { AdviewComponent } from './adview/adview.component';
import { Routes, RouterModule } from "@angular/router";
import { NavComponent } from './nav/nav.component';
import { SingleadComponent } from './singlead/singlead.component';
import { ErrorComponent } from './error/error.component';
import { AddAdComponent } from './add-ad/add-ad.component';
import { HttpClientModule } from '@angular/common/http';
import { ExerciceComponent } from './exercice/exercice.component';

const appRoutes: Routes = [
  { path: "ads" , canActivate:[AuthGuard] ,component: AdviewComponent},
  { path: "ads/:id", canActivate:[AuthGuard], component: SingleadComponent },
  { path: "login", component: AuthComponent },
  { path: "counter_strike", component: ExerciceComponent},
  { path: "add", component: AddAdComponent, canActivate:[AuthGuard]},
  { path: "", component: AdviewComponent, canActivate:[AuthGuard] },
  {path: '404', component: ErrorComponent},
  {path: '**', redirectTo: '/404'}
]


@NgModule({
  declarations: [
    AppComponent,
    AdComponent,
    CounterComponent,
    AuthComponent,
    AdviewComponent,
    NavComponent,
    SingleadComponent,
    ErrorComponent,
    AddAdComponent,
    ExerciceComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    AdService,
    AuthService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
