import { Component, OnInit } from '@angular/core';
import 'rxjs/add/observable/interval';
import { Observable } from 'rxjs/Observable';
import { interval } from 'rxjs/observable/interval';
import { Subject } from 'rxjs/Subject';


@Component({
  selector: 'app-exercice',
  templateUrl: './exercice.component.html',
  styleUrls: ['./exercice.component.scss']
})
export class ExerciceComponent implements OnInit {

  number_secondes: number = 0;
  isClicked: boolean;
  subscription;
  interval;
  count:number = 0;

  constructor() {

   }

  ngOnInit() {
  }

  // counterLaunch(){
  //   this.isClicked = false
  //   let observable = Observable.interval(1000);
  //   let observer = {
  //     next: (value) => {this.number_secondes = value},
  //     error: (error) => {},
  //     complete: () => {},
  //   }
  //    this.subscription = observable.subscribe(observer);

  // }

  // counterStop(){
  //   this.subscription.unsubscribe()
  //   this.number_secondes = 0;
  //   this.isClicked = true;
  // }

  // counterLaunch(){
  //   this.isClicked = false
  //   let observable = Observable.create((observer) => {
  //     observer.next( this.interval = setInterval(() => { this.number_secondes++; this.stop()}, 1000))
  //     observer.complete();
  //   })

  //   this.subscription = observable.subscribe();
  // }

  // stop(){
  //   if(this.number_secondes >= 4){
  //     clearInterval(this.interval);
  //     this.subscription.unsubscribe();
  //   }
  // }

  // counterLaunch(){
  //   this.isClicked = false

  //   let subject = new Subject<any>();

  //   subject.next( this.interval = setInterval(() => { this.number_secondes++; this.stop() }, 1000))
  //   this.subscription = subject.subscribe();
  // }

  // stop(){
  //   if(this.number_secondes >= 4){
  //     clearInterval(this.interval);
  //     this.subscription.unsubscribe();
  //   }
  // }

  counterLaunch(){
    var pas: number;
    this.isClicked = false
    let observable = Observable.create((observer) => {
      observer.next( pas = Math.floor(Math.random() * 3))
      observer.next( this.interval = setInterval(() => { this.number_secondes = this.number_secondes + pas;  this.count++; this.stop() }, 1000))
      observer.complete();
    })

    this.subscription = observable.subscribe();
  }

    stop(){
    if(this.count >= 4){
      clearInterval(this.interval);
      this.subscription.unsubscribe();
    }
  }



}
