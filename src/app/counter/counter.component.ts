import { Component, OnInit, Input, DoCheck } from '@angular/core';
import { AdService } from '../ad.service';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss']
})
export class CounterComponent implements OnInit, DoCheck {

  counter: number;

  constructor(private adService: AdService) { }

  ngOnInit() {
  }

  ngDoCheck() {
    this.counter = this.adService.countPublish();
  }

}
