import { Component, DoCheck } from '@angular/core';
import { AdService } from '../ad.service'

@Component({
  selector: 'app-adview',
  templateUrl: './adview.component.html',
  styleUrls: ['./adview.component.scss']
})
export class AdviewComponent implements DoCheck {

  annonces: any[];
  title = 'Mes annonces !';
  isAuthenticated: boolean = true;

  constructor(private adService: AdService){
  }

  ngDoCheck(){
    this.annonces = this.adService.annonces;
  }

  onPublier() {
    this.adService.publier()
  }
}
